<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $status = random_int(1, 9);
    if(($status%2) == 0){
        $status = 'Активный';
    }else{
        $status = 'Не активный';        
    }
    return [
        'name' => $faker->name,
        'last_name' => $faker->lastName,
        'status' => $status,
//        'phone' => '+380' . $phone,
        'phone' => $faker->phoneNumber,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});
